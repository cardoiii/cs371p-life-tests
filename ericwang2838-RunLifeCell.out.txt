*** Life<Cell> 4x8 ***

Generation = 0, Population = 5.
0-------
------0-
---0----
--00----

*** Life<Cell> 15x7 ***

Generation = 0, Population = 6.
-------
-------
-------
00-0---
-------
------0
-------
-------
--0----
-------
-------
-------
-------
-------
------0

Generation = 3, Population = 43.
00-0---
11--0--
--0--00
.*-1-11
--0-00-
11011--
0---0--
0-0-00-
-0-0-00
0-0-0--
-0-0---
--0---0
-----0-
----0-0
---0-0-

Generation = 6, Population = 47.
00-----
-*---10
*0-0-1-
*.---.*
-*1101-
--.1.10
-11---0
1*-0101
*-0-0-*
10-00-0
0-0-0-*
------0
0-0-0--
-------
0-0----

Generation = 9, Population = 45.
.--0--0
-.0-*-1
*1---.*
*.---.*
-*-*--*
1-...*-
0-*-*-0
-*0-.0*
.-001-*
.---1--
-0-011*
11--0-*
00-0---
--0--0-
-0-0--0

Generation = 12, Population = 47.
.1-11.*
1*-0.-1
.*-1-.*
.*1-1*.
..1.-1.
.-....-
1-*1*-1
-*.-.1.
.-0-*1.
.1---10
*-0111*
*.*-1-.
--*-1-0
0-0-0--
*----1-

*** Life<Cell> 9x14 ***

Generation = 0, Population = 14.
--0-------00--
--------------
------0-------
-00----0------
-0-0----------
--0--------0--
--------------
--------------
--00--------0-

Generation = 2, Population = 51.
0-0-0-0-01**10
-01----0------
-1100---0-00--
1-*000---0-0--
-*-0-00-------
000-0--0-0---0
-01---------0-
--1--------0--
01**10----0-0-

Generation = 4, Population = 48.
01-0--01-1**1*
--*-------0---
-----------0--
1-.0-00----*--
1.0*-0-0--010-
*0----00-----0
--*1--0-----0-
-110---0------
*1**1-100---0-

*** Life<Cell> 15x19 ***

Generation = 0, Population = 14.
--------------0----
--------------0----
---0---------------
-------------------
-----0---0--------0
---------0---------
--0----------------
-----------0-------
-------------------
-------0-----------
-------------------
---0---------------
-----------0-------
0--0---------------
-------------------

Generation = 2, Population = 43.
---0--------0-*-0--
------------0---0--
-0-------0----1---0
---------1----0----
--0--------0----0-0
-------0-----------
0---00---1--------0
-------0-----0-----
--0----------------
---0-0---0-0-------
-----------0-------
00-0-0-0-----------
---------0---0-----
0110-0-------------
-----------0-------

Generation = 4, Population = 43.
---0-0---00---.----
---------10---1---1
--1----0------.----
---------.-1-------
-1---0---*---0--0--
-----1-0-----00----
0--1--1------------
---0---0-------0---
-----1---1-1------0
0--------0-*-------
--0----------------
-------0---1-------
-------1-------0---
*-1*0--------------
-----------0-------

Generation = 6, Population = 103.
-01----0100110.0---
-------11---------1
01-100----1---*00--
-----1-10.----1---0
1-10--11-.----01--0
-----.---*--1---1--
0-1.*1--0--0-1--0-0
10---1-0---1--1--0-
1-0---1--.-*-0--0-0
0-10---0-*0*10-0---
1---00-1-1-----0--0
--1000---0-1-0-----
--11-0-.-0-1-0---0-
.1..-101---1-------
---1---0-0-0-0-0---

Generation = 8, Population = 86.
--.---1-.0-1-**0*0-
------11110-----*--
-.-.-0-0--1101.0*1-
---0-.1--*----1-1--
--.*--.-0*-01-0.-1-
00---.-10.1-----.1-
---..----11--------
.--1-*---0111------
-------10.0.--0----
0--*-----.-*10-*---
*------.-1-1-0-----
----*--*--0.0------
00-1--0.1*-1------1
.1*.*---10-1-------
0111-1-------------

*** Life<Cell> 7x6 ***

Generation = 0, Population = 12.
----00
0-----
--00--
-0-0-0
-----0
--0---
--0-0-

Generation = 1, Population = 30.
0--011
-00000
0-1-00
0-01-1
-00001
-01000
-01--0

*** Life<Cell> 10x12 ***

Generation = 0, Population = 5.
------------
-----0--0---
------------
------------
------------
------------
-----------0
--------0---
-----0------
------------

Generation = 1, Population = 19.
-----0--0---
----0-00-0--
-----0--0---
------------
------------
-----------0
--------0-0-
-----0-0-0-0
----0-0-0---
-----0------

Generation = 2, Population = 17.
------------
---0--11--0-
------------
-----0--0---
-----------0
--------0---
-----0---0-0
------0---0-
---0---0---0
--------0---

Generation = 3, Population = 36.
---0--00--0-
--0-00**00-0
---0-0000-0-
----0-00-0-0
-----0----0-
-----0-0----
----0-----0-
---0-------0
--0-0-------
---0-----0-0

Generation = 4, Population = 17.
------------
-0--11..11--
-----------0
--------0---
-----1------
-----1--0---
-------0-0--
----0-------
-0---0---0--
--------0---

Generation = 5, Population = 44.
-0--00--00--
0-00**..**00
-0--00---00-
-----0-0-0-0
----0*0-----
----0*0-----
----000-0-0-
-0-0---0----
0-0---0---0-
-0---0-0----

Generation = 6, Population = 40.
------00---0
--1-..*...11
-----100-1-0
-0--0-0--10-
---0-.---0-0
---01.100-0-
-0---1-0---0
--0--00-----
-------0-0-0
----0-0-0-0-

*** Life<Cell> 17x7 ***

Generation = 0, Population = 13.
-------
------0
-------
------0
0------
-------
0------
-0-----
--0----
-------
------0
-----0-
------0
0-0----
-0-----
-------
-----0-

Generation = 4, Population = 33.
0------
--0-0-0
0------
-01-0--
----1--
------0
--0-0-1
-0----1
-------
1-0----
10*-0--
-----0-
----*0-
0-0-0-0
-0----*
-----0-
-----01

Generation = 8, Population = 47.
*-1----
-*-----
0---*-1
--.--10
--1-.--
0-.----
-0--*-*
1-0-10.
*1-1-01
**--.--
-*.-00-
--..1-*
1111.-0
1-00.0-
1--*--.
-1--1**
--1-.0*

*** Life<Cell> 18x5 ***

Generation = 0, Population = 12.
-----
0---0
--0--
-----
---0-
-0-0-
----0
----0
-----
-0---
-----
---0-
-----
--0-0
-----
-----
-----
-----

Generation = 1, Population = 35.
0---0
-000-
00-00
--00-
-0010
0--1-
-0--1
---01
-0--0
0-0--
-0-0-
--0-0
--000
-0---
--0-0
-----
-----
-----

Generation = 2, Population = 31.
--0--
0---0
1--11
00-10
-11-1
-0-*0
--00*
--01*
----1
----0
-----
--1-1
-----
0-00-
-----
--0-0
-----
-----

Generation = 3, Population = 36.
00-00
100-1
--0-*
--0-1
-**--
01-.1
----.
-0-*.
--0--
---01
--0--
-0---
0--00
--110
0--00
-0---
--0-0
-----

Generation = 4, Population = 28.
--01-
-1-0-
1---*
----*
-.*1-
1--.-
0--0.
0-0..
---01
--01*
----0
---01
-----
-0*-1
-----
---0-
-----
--0-0

Generation = 5, Population = 41.
--1-0
--01-
----*
0-0-*
0..*-
*10*-
-0--.
1--.*
0-0--
-01*.
--00-
--11-
-000-
01.--
-0000
--0-0
--000
-0---

Generation = 6, Population = 40.
-0*1-
-1-*1
1---*
1--1*
-...-
**-*1
01-0.
*-0..
101-1
-1-..
--110
--**1
--1-0
1*.-1
--1--
-----
-----
0--00

Generation = 7, Population = 47.
0-.--
-*0.-
*--1.
*00**
-**.-
**-**
1-01*
.-1..
-1---
-*1.*
---*-
-0..*
0-*0-
***1*
0--00
--0--
0--00
-0011

Generation = 8, Population = 41.
--.--
1*1.-
*00**
*11.*
0...-
*.-.*
*1-**
.-*..
1*1-1
0***.
---*0
--..*
1-.-0
*..-.
1-1--
-0-0-
--0--
-1-*-

Generation = 9, Population = 36.
00.--
-.-.1
.--.*
.*-.*
-..*-
*.-**
**0**
.0..*
-.*0-
-....
00-.-
0-..*
*0.01
*..-.
*0---
----0
-0-0-
0-0.1

Generation = 10, Population = 46.
11.-0
1.-.*
.0-**
..1**
0.**1
*.-..
*.-..
*1..*
-.*1-
0**..
-11.0
-0***
*1.1-
..*1.
*11-0
0----
--0-0
-1-*-

Generation = 11, Population = 33.
-*.11
*.-.*
.-0..
..-..
1.*.-
*.0*.
*.-..
****.
-..*-
1....
-**.1
-1..*
*-.--
...-*
*--0-
1----
0--0-
0-0*-

Generation = 12, Population = 38.
-..*-
.*1.*
.0-..
.*-..
*.**-
*.-*.
*.-..
****.
-..*1
-***.
-****
-***.
.-.--
...-.
.11--
-0---
-0-10
1-1*1

Generation = 13, Population = 43.
-.**-
.***.
*--..
**-..
*.**1
*.-*.
*.0*.
*****
1...*
1*...
0...*
0*..*
.1*1-
...-.
.-*0-
1--00
-10--
-1*.-

Generation = 14, Population = 34.
-*.*1
.*.*.
*00..
*.-*.
*.***
*.0..
*.-..
*...*
-...*
**...
-....
-...*
*-**-
.*.-.
.1*--
--0-1
----0
1-*.-

*** Life<Cell> 14x20 ***

Generation = 0, Population = 8.
--------------------
-----0-----0--------
--------------------
--------------------
--------------------
---------------0----
--0-----------------
--------------------
--------------------
--------------------
--------------------
--------------0-----
0-------------0-----
---0----------------

Generation = 3, Population = 89.
---0---0-0---0------
--0-0-0---0-0-0-----
---0-0-0-0-0-0-0----
--0-0-0---0-0-0-0---
-0-0-0-----0-0-0-0--
0-0-0-------0-0-0-0-
-0-0-0-------0-0-0--
0-0-0---------0-0---
-0-0----------00----
0-0----------010----
-0-0--------00*00---
----0------0101010--
-0-0-0-----0101010--
--0---0-----00-00---

Generation = 6, Population = 66.
--0-----------------
-----0-----0-0------
0---0---------------
-0-----0-0-0-------0
0-0---0-------------
---0---0------0--0-0
--0-0---0-----1-----
---0-0------0-.10--0
------0-----0-.-0---
-0-0-0----0--01--00-
0-0-------1---.---1-
-0-0-0-00---1--01---
--0-0-0-0----1.1----
---0-0-0-01-0-.-0-1-

Generation = 9, Population = 87.
-------------0-0---0
0-0-0-------0---0-0-
-0-0---------000---0
0------------010----
-0-0-0-0---0-0*0-0--
0-0-0---0---0-1-0-0-
---0---0-0----.--0--
0-0-------0---..----
-0-0-0-----0--.-----
0---0-0---1-01.1----
-----00---.0--.-0-.-
-----0-10-----10.-*0
---0--00.0-111.*-1*0
--0-0---01*110.11-1-

Generation = 12, Population = 68.
------0-------1-----
---------0---*---0--
--0-0-----0---.-----
---0------0--.*.--0-
0---0-----0--1.1--0-
-----0---0---*.*----
0-----0-0---00.-0---
------10----*-..1---
0-0-0---0--1-**----0
-0---1.--01-...--*--
----1----0*01..--0.-
--1--.--*--1.-.-.0*1
0---1-.-.-.-.*.*-..0
-----*--**...-.-*-*1

Generation = 15, Population = 123.
-0---0-0--1------000
--0-0---00-0-*1-00-1
--------1-.--0.0--*-
0-0-0--010-11***-*--
-----00000**-*.*1---
0-0--0-100---*..11*-
---010*-0-101-.-1-0-
0-00-*-1-10-.0...-10
--0-0--01-01**.-1*0-
00-00-.100-...*1-*0-
000-10----...*.---.-
-*111.*-.1-..0.*.-.*
1-0-*-.1...*....*..-
0011-*-*..*..-.-.-.-

*** Life<Cell> 14x10 ***

Generation = 0, Population = 7.
----------
----------
-0--------
---0---0--
----------
----------
----------
----------
-------0--
-0--------
----------
----------
0---------
0---------

Generation = 5, Population = 67.
0-0-------
-----0----
0-0.0-00-0
-.0-0001--
-1-0--0*00
0*0----1--
0*00---1--
00010-0-00
1-0*00-00-
-1000-0--0
10---0----
.-0-0--0--
.-*-100-0-
1**-10-0--

Generation = 10, Population = 58.
-*-0---1--
*-.*-*---1
1-..1-*--.
-.-.*--.1*
-.0-0-0.1.
-*-*0-0.0-
..010--.--
**-------1
*.**-.0---
*-.1-*-0-0
.01-0-010-
.*-0*-0*0-
**.-.*1.11
.*.--*.1--
